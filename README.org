#+OPTIONS: ^:nil
* [[http://kegel.com/c10k.html][C10k problem]]
* Методы ввода-вывода
** [[https://tproger.ru/translations/diversity-of-input-output-io][Основные методы ввода-вывода]]

** [[file:images/nonblocking.png]]

** [[file:images/multiplexing.png]]

** [[file:images/epoll.png]]

* Nginx
** [[https://nginx.org/ru/docs/events.html][Методы обработки соединений]]
** [[https://nginx.com/blog/nginx-vs-apache-our-view][NGINX vs. Apache]]
** [[https://aosabook.org/en/nginx.html][The Architecture of Open Source Applications: nginx]]
* Недостатки epoll
** Неидеальное API (EPOLLONESHOT, EPOLLET, ...)
** Требует системный вызов на каждый дескриптор
** Не обрабатывает файловые дескрипторы
* io_uring
** [[file:images/io-uring.png]]

** [[https://kernel.dk/io_uring.pdf][Efficient IO with io_uring]]
** [[https://unixism.net/loti/what_is_io_uring.html][What is io_uring?]]
** [[https://unixism.net/loti/tutorial/sq_poll.html][Submission queue polling]]

* io_uring и nginx
** [[http://mailman.nginx.org/pipermail/nginx-devel/2020-November/013633.html][Patch]]
** [[https://github.com/hakasenyang/openssl-patch/pull/22][Unofficial patch]]
** [[https://github.com/nginx/unit/issues/511][io_uring support in Nginx unit: planned]]
* sendfile vs io_uring linking
** [[https://man7.org/linux/man-pages/man2/sendfile.2.html][sendfile(2)]]: [[https://en.wikipedia.org/wiki/Direct_memory_access][Direct memory access]]
** [[https://unixism.net/loti/tutorial/link_liburing.html][Linking io_uring requests]]
#+BEGIN_SRC c
int pipes[2];
pipe(pipes);
struct io_uring_sqe* sqe = io_uring_get_sqe(ring);
io_uring_prep_splice(sqe, fd, 0, pipes[1], -1, nbytes, 0);
io_uring_sqe_set_flags(sqe, IOSQE_IO_LINK);

sqe = io_uring_get_sqe(ring);
io_uring_prep_splice(sqe, pipes[0], -1, client_fd, -1, nbytes, 0);
io_uring_sqe_set_flags(sqe, IOSQE_IO_LINK);

sqe = io_uring_get_sqe(ring);
io_uring_prep_close(sqe, pipes[0]);
io_uring_sqe_set_flags(sqe, IOSQE_IO_LINK);

sqe = io_uring_get_sqe(ring);
io_uring_prep_close(sqe, pipes[1]);

io_uring_submit(ring);
#+END_SRC
