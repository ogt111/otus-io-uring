#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "utils.h"
#include "file.h"


khash_t(file_map_t)* file_map;


file_t* get_file(const char* path)
{
    khint_t k = kh_get(file_map_t, file_map, path);
    if(UNLIKELY(k == kh_end(file_map)))
    {
        int fd = open(path, O_RDONLY);
        if(UNLIKELY(fd < 0))
            return NULL;

        struct stat st;
        if(UNLIKELY(fstat(fd, &st) < 0))
        {
            // weird
            close(fd);
            return NULL;
        }

        file_t* file = malloc(sizeof(file_t));
        if(UNLIKELY(!file))
            die("malloc");

        file->fd = fd;
        file->size = st.st_size;


        int r;
        khint_t k = kh_put(file_map_t, file_map, strdup(path), &r);
        kh_value(file_map, k) = file;

        return file;
    }
    else
    {
        return kh_value(file_map, k);
    }
}
